//
//  Utilities.swift
//  OMDB
//
//  Created by Taha Darendeli on 26.07.2019.
//  Copyright © 2019 Taha Darendeli. All rights reserved.
//

import UIKit
import Firebase
import SwiftSpinner

class Utility: NSObject {
    
    static let activityIndicator = UIActivityIndicatorView(style: .whiteLarge)
    
    static func instantiate(name: String, withStoryboard: StoryBoards) -> UIViewController {
        return withStoryboard.get().instantiateViewController(withIdentifier: name)
    }
    
    static func showAlert(title: String, body: String) {
        
    }
    
    static func setGradientBackground(colors: [CGColor], view: UIView, isReverse : Bool = false) {
        
        let gradientLayer = CAGradientLayer()
        if isReverse {
            gradientLayer.colors = colors.reversed()
        } else {
            gradientLayer.colors = colors
        }
        gradientLayer.locations = [0.0, 1.0]
        gradientLayer.frame = view.bounds
        
        view.layer.insertSublayer(gradientLayer, at:0)
    }
    
    static func animateGradient(from: [CGColor], to: [CGColor], view: UIView, duration: Double = 0.5) {
        let gradientLayer = view.layer.sublayers?.first as! CAGradientLayer
        
        let colorsAnimation = CABasicAnimation(keyPath: #keyPath(CAGradientLayer.colors))
        colorsAnimation.fromValue = gradientLayer.colors
        colorsAnimation.toValue = to
        gradientLayer.colors = to
        colorsAnimation.duration = duration
        colorsAnimation.isRemovedOnCompletion = false
        gradientLayer.add(colorsAnimation, forKey: "colors")
    }
    
    static func setOpacityGradient(view: UIView, color: UIColor) {
        let mask = CAGradientLayer()
        mask.startPoint = CGPoint(x: 0.5, y: 0.5)
        mask.endPoint = CGPoint(x: 0.5, y: 1.0)
        mask.colors = [color.withAlphaComponent(1.0).cgColor,color.withAlphaComponent(1.0),color.withAlphaComponent(0.0).cgColor]
        mask.locations = [NSNumber(value: 0.0),NSNumber(value: 0.8),NSNumber(value: 1.0)]
        mask.frame = view.bounds
        view.layer.mask = mask
    }
    
    static func getGradientPair(genre: MovieGenre) -> [CGColor] {
        let gradients = [
            MovieGenre.HORROR : [UIColor(rgb: 0x928DAB).cgColor, UIColor(rgb: 0x1F1C2C).cgColor],
            MovieGenre.ACTION : [UIColor(rgb: 0xC31432).cgColor, UIColor(rgb: 0x240B36).cgColor],
            MovieGenre.ADVENTURE : [UIColor(rgb: 0xe74c3c).cgColor, UIColor(rgb: 0x000000).cgColor],
            MovieGenre.DRAMA : [UIColor(rgb: 0xF3904F).cgColor, UIColor(rgb: 0x3B4371).cgColor],
            MovieGenre.COMEDY : [UIColor(rgb: 0x283c86).cgColor, UIColor(rgb: 0x45a247).cgColor],
            MovieGenre.OTHER : [UIColor(rgb: 0x7296CC).cgColor, UIColor(rgb: 0x444F9B).cgColor]
        ]
        
        return gradients[genre]!
    }
    
    static func serviceUrl() -> String {
        return "http://www.omdbapi.com/?apikey=6d3843c3&"
    }
    
    static func startLoadingSpinner(_ view: UIView){
        SwiftSpinner.show("Loading Data...")
    }
    @objc static func stopLoadingSpinner() {
        SwiftSpinner.hide()
    }
    
    static func setLogEvent(logEventType: LogEventType, parameters: [String : String]) {
        Analytics.logEvent(logEventType.rawValue, parameters: parameters)
    }
    
    static func getGenre(genre: String) -> MovieGenre {
        if genre.contains(MovieGenre.COMEDY.rawValue) {
            return .COMEDY
        } else if genre.contains(MovieGenre.HORROR.rawValue) {
            return .HORROR
        } else if genre.contains(MovieGenre.DRAMA.rawValue) || genre.contains(MovieGenre.ROMANCE.rawValue) {
            return .DRAMA
        } else if genre.contains(MovieGenre.ACTION.rawValue) {
            return .ACTION
        } else if genre.contains(MovieGenre.ADVENTURE.rawValue) {
            return .ADVENTURE
        } else {
            return .OTHER
        }
    }
}

