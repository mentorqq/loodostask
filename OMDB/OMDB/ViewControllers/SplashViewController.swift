//
//  SplashViewController.swift
//  OMDB
//
//  Created by Taha Darendeli on 26.07.2019.
//  Copyright © 2019 Taha Darendeli. All rights reserved.
//

import UIKit
import Reachability
import Firebase
import Motion

class SplashViewController: UIViewController {

    @IBOutlet weak var btnEntrance: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setNavigationBar()
//        updateView()
        
        if isConnectedToNetwork() {
            fetchConfig()
        } else {
            Utility.stopLoadingSpinner()
            Utility.showAlert(title: "", body: "")
        }
    }
    
    func setNavigationBar() {
        Utility.setGradientBackground(colors: Utility.getGradientPair(genre: .OTHER), view: self.view, isReverse: true)
        
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: UIBarMetrics.default)
        self.navigationController?.navigationBar.shadowImage = UIImage()
        self.navigationController?.navigationBar.isTranslucent = true
        self.navigationController?.view.backgroundColor = .clear
        self.navigationController?.navigationBar.tintColor = .white
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor : UIColor.white]
    }
    
    func isConnectedToNetwork() -> Bool {
        
        Utility.startLoadingSpinner(self.view)
        
        let reachability = Reachability()!
        
        if reachability.connection == .none {
            return false
        } else {
            return true
        }
    }
    
    func setupRemoteConfig() {
        let defaultParameters = [
            "connection" : "" as? NSObject
        ]
        
        RemoteConfig.remoteConfig().setDefaults(defaultParameters as! [String : NSObject])
    }
    
    func fetchConfig() {
        
        RemoteConfig.remoteConfig().fetch(withExpirationDuration: 0) { [unowned self] (status, error) in
            
            Utility.stopLoadingSpinner()
            
            guard error == nil else {
                print("Fetch failed")
                return
            }
            
            RemoteConfig.remoteConfig().activate()
            self.updateView()
        }
    }
    
    func updateView() {
        let buttonTitle = RemoteConfig.remoteConfig().configValue(forKey: "connection").stringValue ?? ""
        self.btnEntrance.setTitle(buttonTitle, for: .normal)
        self.btnEntrance.isHidden = false
    }
    
    func rotate(button: UIButton, aCircleTime: Double) { //CABasicAnimation
        
        let rotationAnimation = CABasicAnimation(keyPath: "transform.rotation")
        rotationAnimation.fromValue = 0.0
        rotationAnimation.toValue = -Double.pi * 2 //Minus can be Direction
        rotationAnimation.duration = aCircleTime
        rotationAnimation.repeatCount = .infinity
        button.layer.add(rotationAnimation, forKey: nil)
    }
    
    @IBAction func openMovieSearch(_ sender: Any) {
        let form = Utility.instantiate(name: ViewControllers.MOVIESEARCH.rawValue, withStoryboard: StoryBoards.MAIN)
        self.navigationController?.pushViewController(form, animated: true)
    }
}
