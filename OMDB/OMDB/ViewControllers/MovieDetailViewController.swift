//
//  MovieDetailViewController.swift
//  OMDB
//
//  Created by Taha Darendeli on 26.07.2019.
//  Copyright © 2019 Taha Darendeli. All rights reserved.
//

import UIKit
import Kingfisher

class MovieDetailViewController: UIViewController, UIGestureRecognizerDelegate {
    
    @IBOutlet weak var imgBackground: UIImageView!
    @IBOutlet weak var imgPoster: UIImageView!
    @IBOutlet weak var imgImdb: UIImageView!
    @IBOutlet weak var imgMetascore: UIImageView!
    @IBOutlet weak var posterContainerView: UIView!
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblGenre: UILabel!
    @IBOutlet weak var lblPlot: UILabel!
    @IBOutlet weak var lblImdbRating: UILabel!
    @IBOutlet weak var lblMetascore: UILabel!
    @IBOutlet weak var lblImdbVotes: UILabel!
    @IBOutlet weak var lblDirector: UILabel!
    @IBOutlet weak var lblDate: UILabel!
    @IBOutlet weak var lblActors: UILabel!
    @IBOutlet weak var lblProduction: UILabel!
    @IBOutlet weak var lblWriter: UILabel!
    @IBOutlet weak var lblDuration: UILabel!
    @IBOutlet weak var lblAwards: UILabel!
    @IBOutlet weak var lblLanguage: UILabel!
    @IBOutlet weak var lblCountry: UILabel!
    @IBOutlet weak var btnWebsite: UIButton!
    
    var leftBarButtonItem : UIBarButtonItem!
    var selectedMovie : MovieItem!
    var genre : MovieGenre!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setNavigationBar()
        self.genre = Utility.getGenre(genre: selectedMovie.genre)
        setView()
        setGesture()
        
        Utility.setLogEvent(logEventType: .MOVIE_DETAIL_VIEW, parameters: [
            "title" : selectedMovie.title,
            "year" : selectedMovie.year,
            "rated" : selectedMovie.rated,
            "released" : selectedMovie.released,
            "runtime" : selectedMovie.runtime,
            "genre" : selectedMovie.genre,
            "director" : selectedMovie.director,
            "writer" : selectedMovie.writer,
            "actors" : selectedMovie.actors,
            "plot" : selectedMovie.plot,
            "language" : selectedMovie.language,
            "country" : selectedMovie.country,
            "awards" : selectedMovie.awards,
            "poster" : selectedMovie.poster,
            "metascore" : selectedMovie.metascore,
            "imdbRating" : selectedMovie.imdbRating,
            "imdbVotes" : selectedMovie.imdbVotes,
            "type" : selectedMovie.type,
            "dvd" : selectedMovie.dvd,
            "boxOffice" : selectedMovie.boxOffice,
            "production" : selectedMovie.production,
            "website" : selectedMovie.website,
            "response" : selectedMovie.response,
            ])
    }
    
    func setNavigationBar() {
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: UIBarMetrics.default)
        self.navigationController?.navigationBar.shadowImage = UIImage()
        self.navigationController?.navigationBar.isTranslucent = true
        self.navigationController?.view.backgroundColor = .clear
        self.navigationController?.navigationBar.tintColor = .white
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor : UIColor.white]

        leftBarButtonItem = UIBarButtonItem(image: #imageLiteral(resourceName: "left-arrow"), style: .plain, target: self, action: #selector(popViewController))
        self.navigationItem.setHidesBackButton(true, animated: true)
        self.navigationItem.setLeftBarButton(leftBarButtonItem, animated: false)
    }
    
    func setView() {
        
        Utility.setOpacityGradient(view: imgBackground, color: UIColor(rgb: 0x7296CC))
        Utility.setGradientBackground(colors: Utility.getGradientPair(genre: .OTHER).reversed(), view: self.view)
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.5, execute: {
            Utility.animateGradient(from: Utility.getGradientPair(genre: .OTHER).reversed(),
                                    to: Utility.getGradientPair(genre: self.genre),
                                    view: self.view)
        })
        
        imgBackground.kf.setImage(with: URL(string: selectedMovie.poster))
        imgPoster.kf.setImage(with: URL(string: selectedMovie.poster), placeholder: #imageLiteral(resourceName: "placeholder"),
                              options: [
                                .scaleFactor(UIScreen.main.scale),
                                .transition(.fade(1)),
                                .cacheOriginalImage
            ])
        imgPoster.applyShadowWithCorner(containerView: posterContainerView, cornerRadious: 10.0)
        
        self.lblTitle.text = selectedMovie.title
        self.lblGenre.text = selectedMovie.genre
        self.lblPlot.text = selectedMovie.plot
        self.lblDirector.text = "Directed by \(selectedMovie.director)"
        self.lblDate.text = "Released at \(selectedMovie.released)"
        self.lblActors.text = selectedMovie.actors
        self.lblProduction.text = selectedMovie.production
        self.lblWriter.text = selectedMovie.writer
        self.lblDuration.text = selectedMovie.runtime
        self.lblAwards.text = selectedMovie.awards
        self.lblLanguage.text = selectedMovie.language
        self.lblCountry.text = selectedMovie.country
        adjustWebsiteButton()
        
        if self.selectedMovie.imdbRating == "N/A" {
            self.imgImdb.isHidden = true
            self.lblImdbRating.isHidden = true
            self.lblImdbVotes.isHidden = true
        } else {
            self.lblImdbRating.text = "\(self.selectedMovie.imdbRating)/10"
            self.lblImdbVotes.text = "\(self.selectedMovie.imdbVotes) votes"
        }
        
        if self.selectedMovie.metascore == "N/A" {
            self.imgMetascore.isHidden = true
            self.lblMetascore.isHidden = true
        } else {
            self.lblMetascore.text = "\(self.selectedMovie.metascore)/100"
        }
    }
    
    func setGesture() {
        let swipeDown = UISwipeGestureRecognizer(target: self, action: #selector(popViewController))
        swipeDown.direction = .down
        swipeDown.delegate = self
        containerView.addGestureRecognizer(swipeDown)
    }
    
    func adjustWebsiteButton() {
        let underscoreAttribute: [NSAttributedString.Key: Any] = [
            .font: UIFont.systemFont(ofSize: 14),
            .foregroundColor: UIColor.lightGray,
            .underlineStyle: NSUnderlineStyle.single.rawValue]
        
        let attributeString = NSMutableAttributedString(string: selectedMovie.website,
                                                        attributes: underscoreAttribute)
        btnWebsite.setAttributedTitle(attributeString, for: .normal)
    }
    
    @objc func popViewController() {
        Utility.animateGradient(from: Utility.getGradientPair(genre: self.genre),
                                to: Utility.getGradientPair(genre: .OTHER).reversed(),
                                view: self.view, duration: 0.3)
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.3, execute: {
            self.navigationController?.popViewController(animated: true)
        })
    }
    
    func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldRecognizeSimultaneouslyWith otherGestureRecognizer: UIGestureRecognizer) -> Bool {
        return true
    }
    
    @IBAction func openWebsite(_ sender: Any) {
        guard let url = URL(string: selectedMovie.website) else { return }
        UIApplication.shared.open(url)
    }
}
