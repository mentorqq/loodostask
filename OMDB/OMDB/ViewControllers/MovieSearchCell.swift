//
//  MovieSearchCell.swift
//  OMDB
//
//  Created by Taha Darendeli on 26.07.2019.
//  Copyright © 2019 Taha Darendeli. All rights reserved.
//

import UIKit

class MovieSearchCell: UICollectionViewCell {
    
    @IBOutlet weak var poster: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        contentView.layer.cornerRadius = 20.0
        contentView.layer.borderWidth = 1.0
        contentView.layer.borderColor = UIColor.clear.cgColor
        contentView.layer.masksToBounds = true
        
        self.layer.cornerRadius = 20.0
        layer.shadowRadius = 20
        layer.shadowOpacity = 0.8
        layer.shadowOffset = CGSize(width: 0, height: 30)
        layer.masksToBounds = false
    }
}
