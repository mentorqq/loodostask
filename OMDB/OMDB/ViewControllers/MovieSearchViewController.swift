//
//  MovieSearchViewController.swift
//  OMDB
//
//  Created by Taha Darendeli on 26.07.2019.
//  Copyright © 2019 Taha Darendeli. All rights reserved.
//

import UIKit
import Kingfisher
import Motion

class MovieSearchViewController: UIViewController {

    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var detailView: UIView!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblGenre: UILabel!
    @IBOutlet weak var lblRuntime: UILabel!
    @IBOutlet weak var lblRatingImdb: UILabel!
    @IBOutlet weak var lblYear: UILabel!
    @IBOutlet weak var lblRatingMetascore: UILabel!
    @IBOutlet weak var initialView: UIView!
    @IBOutlet weak var noResultView: UIView!
    @IBOutlet weak var imgReload: UIImageView!
    
    let reuseIdentifier = "cell"
    var rightBarButtonItem : UIBarButtonItem!
    var leftBarButtonItem : UIBarButtonItem!
    var movies : [MovieSearchItem] = []
    var selectedMovie : MovieItem!
    var searchController : UISearchController!
    var searchText = ""
    var pageCounter = 2
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.collectionView.isHidden = true
        self.detailView.isHidden = true
        self.initialView.isHidden = false
        
        setNavigationBar()
        setCollectionView()
        setGesture()
        setLoader()
    }
    
    func setGesture() {
        let swipeUp = UISwipeGestureRecognizer(target: self, action: #selector(handleGesture))
        swipeUp.direction = .up
        self.view.addGestureRecognizer(swipeUp)
    }
    
    func setNavigationBar() {
        Utility.setGradientBackground(colors: Utility.getGradientPair(genre: .OTHER), view: self.view)
        
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: UIBarMetrics.default)
        self.navigationController?.navigationBar.shadowImage = UIImage()
        self.navigationController?.navigationBar.isTranslucent = true
        self.navigationController?.view.backgroundColor = .clear
        self.navigationController?.navigationBar.tintColor = .white
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor : UIColor.white]
        
        rightBarButtonItem = UIBarButtonItem(image: #imageLiteral(resourceName: "search"), style: .plain, target: self, action: #selector(searchTapped))
        leftBarButtonItem = UIBarButtonItem(image: #imageLiteral(resourceName: "left-arrow"), style: .plain, target: self, action: #selector(popViewController))
        
        self.navigationItem.title = "Movies"
        self.navigationItem.setHidesBackButton(true, animated: true)
        self.navigationItem.setRightBarButton(rightBarButtonItem, animated: false)
    }
    
    func setCollectionView() {
        collectionView.delegate = self
        collectionView.dataSource = self
        collectionView.dragInteractionEnabled = true
        
        let cellScaling : CGFloat = 0.6
        
        let screenSize = UIScreen.main.bounds.size
        let cellWidth = floor(screenSize.width * cellScaling)
        let cellHeight = floor(screenSize.height * cellScaling)
        
        let insetX = (view.bounds.width - cellWidth) / 2.0
        let insetY = (view.bounds.height - cellHeight) / 2.0
        
        let layout = collectionView.collectionViewLayout as! UICollectionViewFlowLayout
        layout.itemSize = CGSize(width: cellWidth, height: cellHeight)
        collectionView.contentInset = UIEdgeInsets(top: 100, left: insetX, bottom: insetY, right: insetX)
        layout.minimumLineSpacing = 50
    }
    
    func setLoader() {
        imgReload.rotate(firstValue: -(.pi * 2), secondValue: (.pi / 10))
    }
    
    @objc func searchTapped() {
        searchController = UISearchController(searchResultsController: nil)
        searchController.delegate = self
        searchController.searchResultsUpdater = self
        searchController.dimsBackgroundDuringPresentation = true
        definesPresentationContext = true
        searchController.loadViewIfNeeded()
        searchController.searchBar.delegate = self
        searchController.hidesNavigationBarDuringPresentation = false
        searchController.searchBar.placeholder = "Search place"
        searchController.searchBar.sizeToFit()
        searchController.searchBar.barTintColor = navigationController?.navigationBar.barTintColor
        searchController.searchBar.tintColor = .white
        searchController.searchBar.showsCancelButton = true
        
        self.navigationItem.titleView = searchController.searchBar
        self.navigationItem.rightBarButtonItem = nil
        self.navigationItem.leftBarButtonItem = nil
        
        
        UIView.animate(withDuration: 0.2, animations: {
            self.initialView.alpha = 0.0
            self.initialView.isHidden = true
        })
    }
    
    @objc func popViewController() {
        self.navigationController?.popViewController(animated: true)
    }
    
    @objc func handleGesture() {
        if !collectionView.isDecelerating && !collectionView.isDragging && self.selectedMovie != nil {
            let form = Utility.instantiate(name: ViewControllers.MOVIEDETAIL.rawValue, withStoryboard: StoryBoards.MAIN) as! MovieDetailViewController
            form.selectedMovie = self.selectedMovie
            self.navigationController?.pushViewController(form, animated: true)
        }
    }
    
    func getMovieDetails(by id: String) {
        MovieItem.getItem(searchText: id, result: { movie in
            if let movie = movie {
                self.selectedMovie = movie
                
                DispatchQueue.main.asyncAfter(deadline: .now() + 0.1, execute: {
                    self.lblTitle.text = self.selectedMovie.title
                    self.lblYear.text = self.selectedMovie.year
                    self.lblGenre.text = self.selectedMovie.genre
                    self.lblRuntime.text = self.selectedMovie.runtime
                    self.lblRatingImdb.text = self.selectedMovie.imdbRating
                    self.lblRatingMetascore.text = self.selectedMovie.metascore
                    self.detailView.isHidden = false
                    
                    Utility.stopLoadingSpinner()
                })
            }
        })
    }
    
    func showNoResult() {
        self.noResultView.alpha = 0.0
        self.noResultView.isHidden = false
        
        UIView.animate(withDuration: 0.2, animations: {
            self.noResultView.alpha = 1.0
            self.collectionView.alpha = 0.0
            self.detailView.alpha = 0.0
        }, completion: { _ in
            self.collectionView.isHidden = true
            self.detailView.isHidden = true
        })
        
        Utility.stopLoadingSpinner()
    }
}

extension MovieSearchViewController: UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return movies.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: reuseIdentifier,
                                                      for: indexPath) as! MovieSearchCell
        
        let posterURL = URL(string: movies[indexPath.row].posterUrl)
//        let placeholder = #imageLiteral(resourceName: "placeholder")
        cell.poster.kf.setImage(with: posterURL, placeholder: #imageLiteral(resourceName: "placeholder"),
                                options: [
                                    .scaleFactor(UIScreen.main.scale),
                                    .transition(.fade(1)),
                                    .cacheOriginalImage
                                ])
        
        return cell
    }
    
    
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        
        if indexPath.row == movies.count - 1 {
            
            MovieSearchItem.getList(searchText: searchText + "&page=\(pageCounter)", result: { result in
                if let result = result {
                    self.movies += result
                }
                self.collectionView.reloadData()
                self.pageCounter += 1
            })
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let form = Utility.instantiate(name: ViewControllers.MOVIEDETAIL.rawValue, withStoryboard: StoryBoards.MAIN) as! MovieDetailViewController
        form.selectedMovie = self.selectedMovie
        self.navigationController?.pushViewController(form, animated: true)
    }
    
    func scrollViewWillBeginDragging(_ scrollView: UIScrollView) {
        UIView.animate(withDuration: 0.2, animations: {
            self.detailView.alpha = 0.0
        })
    }
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        UIView.animate(withDuration: 0.3, animations: {
            self.detailView.alpha = 1.0
        })
    }
    
    func scrollViewWillEndDragging(_ scrollView: UIScrollView, withVelocity velocity: CGPoint, targetContentOffset: UnsafeMutablePointer<CGPoint>) {
        let layout = self.collectionView.collectionViewLayout as! UICollectionViewFlowLayout
        let cellWidthIncludingSpacing = layout.itemSize.width + layout.minimumLineSpacing
        
        var offset = targetContentOffset.pointee
        let index = (offset.x + scrollView.contentInset.left) / cellWidthIncludingSpacing
        let roundedIndex = round(index)
        
        offset = CGPoint(x: roundedIndex * cellWidthIncludingSpacing - scrollView.contentInset.left, y: -scrollView.contentInset.top)
        targetContentOffset.pointee = offset
        
        getMovieDetails(by: self.movies[Int(roundedIndex)].imdb)
    }
}

extension MovieSearchViewController: UISearchControllerDelegate, UISearchResultsUpdating, UISearchBarDelegate {
    func updateSearchResults(for searchController: UISearchController) {
        
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        
        Utility.startLoadingSpinner(self.view)
        
        searchController.dismiss(animated: true, completion: {
            self.navigationItem.titleView = nil
            self.navigationItem.title = "Movies"
        })
        
        self.searchText = searchBar.text!
        
        UIView.animate(withDuration: 0.2, animations: {
            self.collectionView.alpha = 0.0
        }, completion: { _ in
            if self.movies.count > 0 {
                self.collectionView.scrollToItem(at: IndexPath(row: 0, section: 0), at: .left, animated: true)
            }
        })
        
        MovieSearchItem.getList(searchText: searchBar.text!, result: { result in
            if let result = result {
                self.movies = result
                
                if self.movies.count != 0 {
                    self.getMovieDetails(by: self.movies.first!.imdb)
                    
                    self.collectionView.isHidden = false
                    self.detailView.isHidden = false
                    
                    UIView.animate(withDuration: 0.2, animations: {
                        self.noResultView.alpha = 0.0
                        self.collectionView.alpha = 1.0
                        self.detailView.alpha = 1.0
                    }, completion: { _ in
                        self.noResultView.isHidden = true
                    })
                } else {
                    self.showNoResult()
                }
            } else {
                self.showNoResult()
            }
            self.imgReload.isHidden = true
            self.collectionView.reloadData()
        })
    }

    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        self.navigationItem.titleView = nil
        self.navigationItem.title = "Movies"
        self.navigationItem.setRightBarButton(rightBarButtonItem, animated: true)
        
        if searchText == "" {
            self.initialView.alpha = 0.0
            self.initialView.isHidden = false
            UIView.animate(withDuration: 0.2, animations: {
                self.initialView.alpha = 1.0
            })
        }
    }
    
    func searchBarTextDidEndEditing(_ searchBar: UISearchBar) {
        self.navigationItem.titleView = nil
        self.navigationItem.title = "Movies"
        self.navigationItem.setRightBarButton(rightBarButtonItem, animated: true)
    }
}
