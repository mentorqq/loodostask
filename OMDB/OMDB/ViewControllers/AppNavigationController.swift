//
//  AppNavigationController.swift
//  OMDB
//
//  Created by Taha Darendeli on 27.07.2019.
//  Copyright © 2019 Taha Darendeli. All rights reserved.
//
import UIKit
import Material

class AppNavigationController: NavigationController {
    open override func prepare() {
        super.prepare()
        isMotionEnabled = true
        motionNavigationTransitionType = .slide(direction: .down)
        motionNavigationTransitionType = .autoReverse(presenting: .slide(direction: .up))
        guard let v = navigationBar as? NavigationBar else {
            return
        }
        
        v.backgroundColor = .white
        v.depthPreset = .none
        v.dividerColor = Color.grey.lighten2
    }
}
