//
//  MovieItem.swift
//  OMDB
//
//  Created by Taha Darendeli on 26.07.2019.
//  Copyright © 2019 Taha Darendeli. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON

class MovieItem: NSObject {
    var title = ""
    var year = ""
    var rated = ""
    var released = ""
    var runtime = ""
    var genre = ""
    var director = ""
    var writer = ""
    var actors = ""
    var plot = ""
    var language = ""
    var country = ""
    var awards = ""
    var poster = ""
    var metascore = ""
    var imdbRating = ""
    var imdbVotes = ""
    var imdbId = ""
    var type = ""
    var dvd = ""
    var boxOffice = ""
    var production = ""
    var website = ""
    var response = ""
    
    class func getItem(searchText: String, result:@escaping (MovieItem?)->()){
        
        var item = MovieItem()
        let urlString = Utility.serviceUrl() + "i=\(searchText)"
        
        Alamofire.request(urlString).responseJSON { response in
            
            if let result = response.result.value {
                let json = JSON(result)
                print("JSON: \(json)") // serialized json response
                
                item = parseJson(json: json)
            }
            result(item)
        }
    }
    
    class func parseJson(json : JSON) -> MovieItem{
        let ent = MovieItem()
        
        ent.title = json["Title"].stringValue
        ent.year = json["Year"].stringValue
        ent.rated = json["Rated"].stringValue
        ent.released = json["Released"].stringValue
        ent.runtime = json["Runtime"].stringValue
        ent.genre = json["Genre"].stringValue
        ent.director = json["Director"].stringValue
        ent.writer = json["Writer"].stringValue
        ent.actors = json["Actors"].stringValue
        ent.plot = json["Plot"].stringValue
        ent.language = json["Language"].stringValue
        ent.country = json["Country"].stringValue
        ent.awards = json["Awards"].stringValue
        ent.poster = json["Poster"].stringValue
        ent.metascore = json["Metascore"].stringValue
        ent.imdbRating = json["imdbRating"].stringValue
        ent.imdbVotes = json["imdbVotes"].stringValue
        ent.imdbId = json["imdbId"].stringValue
        ent.type = json["Type"].stringValue
        ent.dvd = json["DVD"].stringValue
        ent.boxOffice = json["BoxOffice"].stringValue
        ent.production = json["Production"].stringValue
        ent.website = json["Website"].stringValue
        ent.response = json["Response"].stringValue
        
        return ent
    }
}
