//
//  MovieSearchItem.swift
//  OMDB
//
//  Created by Taha Darendeli on 26.07.2019.
//  Copyright © 2019 Taha Darendeli. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON

final class MovieSearchItem: NSObject {
    
    var title = ""
    var year = ""
    var imdb = ""
    var type = ""
    var posterUrl = ""
    
    override init() {
    }
    
    class func getList(searchText: String, result:@escaping ([MovieSearchItem]?)->()){
        
        var list = [MovieSearchItem]()
        let urlString = Utility.serviceUrl() + "s=\(searchText)"
        
        Alamofire.request(urlString).responseJSON { response in
            
            if let response = response.result.value {
                let json = JSON(response)
                print("JSON: \(json)") // json response
                
                for i in 0..<json["Search"].count {
                    let item = json["Search"][i]
                    list.append(parseJson(json: item))
                }
                result(list)
                
                return
            }
            result(nil)
        }
    }
    
    class func parseJson(json : JSON) -> MovieSearchItem{
        let ent = MovieSearchItem()
        
        ent.title = json["Title"].stringValue
        ent.year = json["Year"].stringValue
        ent.imdb = json["imdbID"].stringValue
        ent.type = json["Type"].stringValue
        ent.posterUrl = json["Poster"].stringValue
        
        return ent
    }
}
