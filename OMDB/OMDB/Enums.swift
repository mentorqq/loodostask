//
//  Enums.swift
//  OMDB
//
//  Created by Taha Darendeli on 26.07.2019.
//  Copyright © 2019 Taha Darendeli. All rights reserved.
//
import UIKit

public enum StoryBoards : String {
    case MAIN = "Main"
    
    func get() -> UIStoryboard {
        return UIStoryboard(name: self.rawValue, bundle: nil)
    }
}

public enum ViewControllers : String {
    case SPLASH = "splashvc"
    case MOVIESEARCH = "movie_search"
    case MOVIEDETAIL = "movie_detail"
}

public enum LogEventType: String {
    case MOVIE_DETAIL_VIEW = "Movie Detail Seen"
}

public enum MovieGenre: String {
    case HORROR = "Horror"
    case ACTION = "Action"
    case ADVENTURE = "Adventure"
    case DRAMA = "Drama"
    case COMEDY = "Comedy"
    case ROMANCE = "Romance"
    case OTHER
}
