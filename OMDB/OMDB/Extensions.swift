//
//  Extensions.swift
//  OMDB
//
//  Created by Taha Darendeli on 26.07.2019.
//  Copyright © 2019 Taha Darendeli. All rights reserved.
//
import UIKit

class Extensions: NSObject {
    
}

extension UIColor {
    convenience init(red: Int, green: Int, blue: Int) {
        assert(red >= 0 && red <= 255, "Invalid red component")
        assert(green >= 0 && green <= 255, "Invalid green component")
        assert(blue >= 0 && blue <= 255, "Invalid blue component")
        
        self.init(red: CGFloat(red) / 255.0, green: CGFloat(green) / 255.0, blue: CGFloat(blue) / 255.0, alpha: 1.0)
    }
    
    convenience init(rgb: Int) {
        self.init(
            red: (rgb >> 16) & 0xFF,
            green: (rgb >> 8) & 0xFF,
            blue: rgb & 0xFF
        )
    }
}

extension UIImage {
    // image with rounded corners
    public func withRoundedCorners(radius: CGFloat? = nil) -> UIImage? {
        let maxRadius = min(size.width, size.height) / 2
        let cornerRadius: CGFloat
        if let radius = radius, radius > 0 && radius <= maxRadius {
            cornerRadius = radius
        } else {
            cornerRadius = maxRadius
        }
        UIGraphicsBeginImageContextWithOptions(size, false, scale)
        let rect = CGRect(origin: .zero, size: size)
        UIBezierPath(roundedRect: rect, cornerRadius: cornerRadius).addClip()
        draw(in: rect)
        let image = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return image
    }
}

extension UIImageView {
    func applyShadowWithCorner(containerView : UIView, cornerRadious : CGFloat){
        containerView.clipsToBounds = false
        containerView.layer.shadowColor = UIColor.black.cgColor
        containerView.layer.shadowOpacity = 1
        containerView.layer.shadowOffset = CGSize.zero
        containerView.layer.shadowRadius = 10
        containerView.layer.cornerRadius = cornerRadious
        containerView.layer.shadowPath = UIBezierPath(roundedRect: containerView.bounds, cornerRadius: cornerRadious).cgPath
        self.clipsToBounds = true
        self.layer.cornerRadius = cornerRadious
    }
}

extension UIView{
    func rotate(firstValue : Double = Double.pi * 2, secondValue : Double = 0.0) {
        let firstRotation : CABasicAnimation = CABasicAnimation(keyPath: "transform.rotation.z")
        firstRotation.toValue = firstValue
        firstRotation.duration = 1
        firstRotation.repeatCount = Float.greatestFiniteMagnitude
        self.layer.add(firstRotation, forKey: "firstRotation")
        
        let secondRotation : CABasicAnimation = CABasicAnimation(keyPath: "transform.rotation.z")
        secondRotation.toValue = secondValue
        secondRotation.duration = 1
        secondRotation.isCumulative = true
        secondRotation.repeatCount = Float.greatestFiniteMagnitude
        self.layer.add(secondRotation, forKey: "secondRotation")
    }
}

